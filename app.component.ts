import { Component, OnInit } from '@angular/core';
import { Stock } from './models/stock';
import { StockSymbolService } from './stock-symbol.service'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'lalalalalalalalalalalala';

  oneStock : Stock = new Stock();

  portfolio = [{
    symbol: 'zzz', price: 250.75, qty: 1200
  }, {
    symbol: 'yyy', price: 165.75, qty: 165
  }, {
    symbol: 'xxx', price: 819.75, qty: 15641
  }];

  picUrl = "https://placehold.it/120x60";

  editing = "";

  symbolsModel;

  selectedSymbol;
  

  editEventHandle(data){
    this.editing = data;
  }

  constructor (private stockSymbolService : StockSymbolService) {

  }

  ngOnInit() {
    this.stockSymbolService.getSymbols()
      .subscribe( (results) => {
        this.symbolsModel = results.slice(1, 8)
      })
  }


}
