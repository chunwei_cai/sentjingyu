import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Stock } from './models/stock';

@Injectable({
  providedIn: 'root'
})
export class StockSymbolService {

  stockUrl = "https://api.iextrading.com/1.0/ref-data/symbols?filter=symbol,name,isEnabled"

  constructor(private http : HttpClient) {

  }

  // getSymbols(){
  //   return this.http.get(this.stockUrl);
  // }

  getSymbols():Observable<Stock[]>{
    return this.http.get<Stock[]>(this.stockUrl);
  }

}
